import os
from multiprocessing import Process
from pytube import YouTube

script_path = os.path.dirname(os.path.realpath(__file__))

videos_download_path = script_path + "/" + "videos"
links_path = script_path + "/" + "links"
txt_links = links_path + "/" + "ytlinks.txt"

txt_lines = []

video_lenght = []
video_title = []
video_size = []
proc_id = []


#wielowątkowość używać tylko na samej górze!!!

def read_txt_file():
    txt_file = open(txt_links, "r")
    txt_document = txt_file.readlines()
    for everyline in txt_document:
        txt_lines.append(everyline)
    txt_file.close()

def print_download_folder():
    print("Your videos will be downloaded to: {}".format(get_videos_path()))

def get_videos_path():
    return videos_download_path

def get_process_id():
    for everyline in txt_lines:
        proc_id.append(os.getppid())
        print(proc_id)

def get_video_title(everyline):
        title = YouTube(everyline).title
        return title

def save_video_title(everyline):
        video_title.append(get_video_title(everyline))

def get_video_lenght(everyline):
    lenght = int(YouTube(everyline).length) / 60.00
    rounded_lenght = round(lenght, 2)
    return rounded_lenght

def save_video_lenght(everyline):
    video_lenght.append(get_video_lenght(everyline))

def get_video_size(everyline):
    size = (YouTube(everyline).streams.first().filesize)/1000000.00
    rounded_size = round(size, 2)
    return rounded_size

def save_video_size(everyline):
    video_size.append(get_video_size(everyline))

def download_from_txt(link):
        YouTube(link).streams.first().download(videos_download_path)
        process_id = os.getpid()
        print(f"Process ID: {process_id}")

def multiprocess_download_from_txt():
    # this code will spawn one process per each element in list
    for link in txt_lines:
        process = Process(target=download_from_txt, args=(link,))
        process.start()

def save_video_info():
    for everyline in txt_lines:
        save_video_title(everyline)
        save_video_lenght(everyline)
        save_video_size(everyline)

def print_video_info():
    i = len(txt_lines) - 1
    for everyline in txt_lines:
        print(str(video_title[i]) + " " + str(video_lenght[i]) + " m " + str(video_size[i]) + " MB")
        i = i - 1

def start():
    try:
        read_txt_file()
        print_download_folder()
        save_video_info()
        print_video_info()
        try:
            multiprocess_download_from_txt()
        except:
            "A problem with downloading videos has occured."
    except:
        print("Some kind of error has occured. Please restart the script.")





def main():
    start()

    #multiprocess_get_video_lenght()
    #multiprocess_print_video_names()
    #multiprocess_download_from_txt()

if __name__ == '__main__':
    main()


